# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ""
  str.each_char do |chr|
    next if ("a".."z").include?(chr)
    new_str << chr
  end
  new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  odd_middle = str.length / 2
  if str.length % 2 != 0
    str[odd_middle]
  else
    "#{str[odd_middle - 1]}#{str[odd_middle]}"
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char {|ch| vowel_count += 1 if VOWELS.include?(ch)}
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorialized = num
  (2...num).each {|digit| factorialized *= digit}
  factorialized
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr[0...-1].each {|el| str << "#{el}#{separator}"}
  str << "#{arr[-1]}"
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.chars.each_index do |i|
    if i.odd?
      weird_str << str[i].upcase
    else
      weird_str << str[i].downcase
    end
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed_words = []
  arr = str.split
  arr.each do |word|
    if word.length >=5
      reversed_words << word.reverse
    else
      reversed_words << word
    end
  end
  reversed_words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 != 0
      array << "fizz"
    elsif num % 3 != 0 && num % 5 == 0
      array << "buzz"
    elsif num % 3 == 0 && num % 5 == 0
      array << "fizzbuzz"
    else
      array << num
    end
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  (1..arr.count).each {|el| new_arr.unshift(el)}
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |n|
    if num % n == 0
      return false
    end
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sorted_factors = []
  (1..num).each do |n|
    if num % n == 0
      sorted_factors << n
    end
  end
  sorted_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  sorted_factors = []
  (1..num).each do |n|
    if num % n == 0 && prime?(n)
      sorted_factors << n
    end
  end
  sorted_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each do |el|
    if el.odd?
      odds << el
    else
      evens << el
    end
  end
  if odds.count == 1
    odds[0]
  else
    evens[0]
  end
end
        
